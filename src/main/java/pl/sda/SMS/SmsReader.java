package pl.sda.SMS;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class SmsReader {
    public static void main(String[] args) {
        File odczytPliku = new File("sms.txt");
        try {
            Scanner scanner = new Scanner((odczytPliku));
            int liczbaLiter = 0;
            while (scanner.hasNextLine()) {
                String tekst = scanner.nextLine();
                String words[] = tekst.split(" ");
                String sms = "";
                int index = 0;
                for (String word : words) {
                    String[] letters = word.split("");
                    letters[0] = letters[0].toUpperCase();
                    for (int i = 0; i < letters.length; i++) {
                        sms += letters[i];
                        if (liczbaLiter % 160.0 == 0) {
                            liczbaLiter = liczbaLiter + 7;
                        } else{
                            liczbaLiter++;
                        }
                    }
                }
            }
            System.out.println(liczbaLiter);

            int maxIloscZnakow = 160;
            int liczbaSms = (liczbaLiter / maxIloscZnakow) + 1;
            if (liczbaSms <= 10) {
                System.out.println(liczbaSms);
            }else{
                System.out.println("Trzeba zmienić na mms.");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
