package pl.sda.Pesel;

public class Pesel {
    public static void main(String[] args) {
        System.out.println(peselChecker("90090515836"));
    }

    public static boolean peselChecker(String s) {
        String[] liczbyPesel = s.split("");
        int[] liczbyMnozenie = new int[]{1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int suma = 0;
        if (liczbyPesel.length!=11)return false;
        for (int i = 0; i < liczbyMnozenie.length; i++) {
            suma += liczbyMnozenie[i] * Integer.valueOf(liczbyPesel[i]);
        }
        String[] b = String.valueOf(suma).split("");
        int szukana = 10 - Integer.valueOf(b[b.length - 1]);
        if (szukana == Integer.valueOf(liczbyPesel[liczbyPesel.length - 1])) {
            return true;
        } else return false;
    }
}
