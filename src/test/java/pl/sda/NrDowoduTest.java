package pl.sda;

import org.junit.Test;
import pl.sda.NrDowodu.NrDowodu;

import static org.junit.Assert.assertEquals;

public class NrDowoduTest {


    @Test
    public void simpleTest(){
        boolean result = NrDowodu.serialNumber("ABA300000");
        assertEquals(true,result);
    }
    @Test
    public void simpleTest1(){
        boolean result = NrDowodu.serialNumber("ABA300300");
        assertEquals(false,result);
    }
    @Test
    public void shouldReturnFalseForToShortString(){
        boolean result = NrDowodu.serialNumber("ABA300");
        assertEquals(false,result);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerException(){NrDowodu.serialNumber(null);}
    @Test
    public void shouldReturnFalseForToLongString(){
        boolean result = NrDowodu.serialNumber("ABA300342342342342342344");
        assertEquals(false,result);
    }
}
