package pl.sda;

import org.junit.Test;
import pl.sda.NrDowodu.NrDowodu;
import pl.sda.Pesel.Pesel;

import static org.junit.Assert.assertEquals;

public class PeselTest {
    @Test
    public void shouldReturnTrueForCorrectPesel(){
        boolean result = Pesel.peselChecker("90090515836");
        assertEquals(true,result);
    }
    @Test
    public void shouldReturnFalseForIncorrectPesel(){
        boolean result = Pesel.peselChecker("90909090900");
        assertEquals(false,result);
    }
    @Test
    public void shouldReturnFalseForToShortString(){
        boolean result = Pesel.peselChecker("123300");
        assertEquals(false,result);
    }
    @Test
    public void shouldReturnFalseForToLongString(){
        boolean result = Pesel.peselChecker("675300342342342342342344");
        assertEquals(false,result);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowNumberFormatException(){Pesel.peselChecker("9090we90900");}

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerException(){Pesel.peselChecker(null);}
}
